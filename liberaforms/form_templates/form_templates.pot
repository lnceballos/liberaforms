# Translations template for PROJECT.
# Copyright (C) 2020 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2020-06-23 11:50+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.3.4\n"

#: form_templates/form_templates.py:26 form_templates/form_templates.py:29
msgid "One day congress"
msgstr ""

#: form_templates/form_templates.py:34
msgid "Room 1. Municipal Strategies"
msgstr ""

#: form_templates/form_templates.py:35
msgid "Room 2. Decentralized tech. Freedom of speech"
msgstr ""

#: form_templates/form_templates.py:46
msgid "Vegan"
msgstr ""

#: form_templates/form_templates.py:47
msgid "Vegaterian"
msgstr ""

#: form_templates/form_templates.py:48
msgid "I'm eating else where"
msgstr ""

#: form_templates/form_templates.py:55 form_templates/form_templates.py:58
msgid "Summer courses"
msgstr ""

#: form_templates/form_templates.py:56
msgid ""
"Students can enroll in a variety of activities spread out across three "
"days."
msgstr ""

#: form_templates/form_templates.py:65
msgid "Tuesday 25th"
msgstr ""

#: form_templates/form_templates.py:87 form_templates/form_templates.py:90
msgid "Save our shelter"
msgstr ""

#: form_templates/form_templates.py:88
msgid "Petition citizen support for your local initiative."
msgstr ""

#: form_templates/form_templates.py:92
msgid "ID number"
msgstr ""

#: form_templates/form_templates.py:93
msgid "Email"
msgstr ""

#: form_templates/form_templates.py:94
msgid "Comments"
msgstr ""

