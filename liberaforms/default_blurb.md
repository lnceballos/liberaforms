# Libera forms

We have built this software with the hope it will be used by our neighbours, friends, and anyone else who feels GAFAM already has way too much data on all of us.

Did you know Google knows:

* Who our friends and families are.
* Where we are, who we are with, and what we're talking about.
* Our concerns, joys, intereses and plans.
* Our opinion on all sorts of different topics.

Google knows all these things about us, and the same about all the other people we know too.

## Do not feed the dictator!

What sort of things does Google do with the power we give it?

* [Manipulate our thoughts](https://spreadprivacy.com/google-filter-bubble-study/)
* [Sell our data](https://www.eff.org/deeplinks/2020/03/google-says-it-doesnt-sell-your-data-heres-how-company-shares-monetizes-and)
* [Work together with government surveillance programs](https://en.wikipedia.org/wiki/PRISM_(surveillance_program))
* [Sell technology to the war machine](https://www.theguardian.com/technology/2018/mar/07/google-ai-us-department-of-defense-military-drone-project-maven-tensorflow)
* [Invest in oil](https://hooktube.com/watch?v=v3n8txX3144)
* [Sponsor conferences with climate change deniers](https://www.theguardian.com/environment/2019/oct/11/google-contributions-climate-change-deniers)
* [Quash ideas it doesn't like](https://gizmodo.com/yes-google-uses-its-power-to-quash-ideas-it-doesn-t-li-1798646437)

## Enough is enough!

Degoogleizing the world is a massive undertaking so let's start with the low hanging fruit.

Gmail and Drive have locked in millions of users around the world and now they "just can't live without" their accounts and will not easily change to other tools that respect them as people.

However, **people are not invested in Google forms** to the same extent.

Sovereign form software can keep a lot of information away from those googly eyes.
